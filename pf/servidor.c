#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <netdb.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <time.h>
#include <strings.h>
#include <arpa/inet.h>
#include "basic.h"
#include "socket_helper.h"

// General definitions
#define MAX 4096
#define EXIT_COMMAND "exit\n"
#define MIN_CHR 256

// Game specific phrases
#define MENU "Bem vindo ao jogo da forca!\n-----\n 1) Iniciar partida simples\n 2) Ser carrasco ao iniciar partida\n 3) Jogar no modo multiplayer\n"

#define HELP "Bem vindo ao suporte do jogo da forca!\n------\nEsse jogo tem como objetivo adivinhar palavras com menor número de tentativas.\n\nDois personagems principais fazem parte deste jogo: Carrasco e Enforcado.\nO carrasco será o que fornecerá a palavra a ser adivinhada, além de verificar se suas tentativas estão corretas.\nO enforcado será o que tentará adivinhar a palavra através de letras ou palavra.\nModos de jogo:\n-Single player: Computador irá escolher uma palavra aleatória e enforcado deve tentar adivinhar. Você possuirá 6 vidas.\n-Multiplayer: Um jogador irá ser o carrasco enquanto outros jogadores serão os enforcados. Você possuirá 4 vidas.\n\nDesenvolvedores: Alan Carvalho Corrales e Raphael Pontes Santana.\nAgradecemos seu interesse por jogar nosso jogo.\n \0"
#define START_SINGLE_MSG "A partida de jogo da forca começou!\n-----\nVocê possui 6 vidas\n\0"

#define PLAY_AGAIN_MSG "Você gostaria de jogar outra partida?\n[1] Sim\n[2] Não\nDigite o número correspondente:\n\0"

#define ALPHABET "ABCDEFGHIJKLMNOPQRSTUVWXYZ\0"
#define ALPHABET_SZ 26

#define WIN 0
#define LOSE 1
#define IN_PLAY 2

#define ASCII_OFFSET 65

void print_conn_data(int connfd, struct sockaddr_in clientaddr);
char ** read_dictionary(int * lines);
char * select_word(char ** words, int wrd_count);
void send_msg(int connfd, char msg[]);
void get_mode(int *mode, int connfd);
char * update_fields(int n);
char * update_letters(char letters[], char letter_to_remv, bool *checkremove, bool *out_of_range);
char * update_partial(char selected_word[], char picked, char partial[], bool *hit, int *count);
void check_guess(char recvline[], char selected_word[], int *lifes, int *count);
int update_state(int life, int count, int numberletters);
void startgame(int connfd, char selected_word[], bool *play_again);
void singleplayer(int connfd, char selected_word[], bool **play_again);
void checkwin(int state, int connfd, char selected_word[]);
void doit(int connfd, struct sockaddr_in clientaddr, char selected_word[]);
void help();

bool * words_availability;

int main (int argc, char **argv) {
   int    listenfd, // listen socket
          connfd,		// accept socket
          port,			// port number
					LISTENQ;	// Max connections
   struct sockaddr_in servaddr;		// server address
   char   error[MAX + 1];
	 char ** words;		// array of lines
	 int wrd_count = 0;		// file line counter
	 char selected_word[MIN_CHR];
	 time_t t;

	 // Seed random with time
   srand((unsigned)time(&t));

	 // Check argument number
   if (argc != 3) {
		 if(argc ==4){
			 if(strcmp(argv[3], "-h")==0){
				 	help();
			 }
		 }else{
	    strcpy(error,"uso: ");
	    strcat(error,argv[0]);
	    strcat(error," <Port>");
	    perror(error);
	    exit(1);
		}
   }

	 // Read dictionary from file
	 words = read_dictionary(&wrd_count);
	 // Create an array of booleans for word checking
	 words_availability = (bool *)malloc(wrd_count * sizeof(bool));
	 // Initialize booleans
 	 for(int i = 0; i < wrd_count; i++) words_availability[i] = true;
	 // Read port
   port = atoi(argv[1]);
	 // Read connection sockets amount at a time
   LISTENQ = atoi(argv[2]);
	 // Create listen socket and receive descriptor
   listenfd = Socket(AF_INET, SOCK_STREAM, 0);
	 // Get server address structure
   servaddr = ServerSockaddrIn(AF_INET, INADDR_ANY, port);
	 // Bind listen socket to address
   Bind(listenfd, (struct sockaddr *) &servaddr, sizeof(servaddr));
	 // Listen to the socket
   Listen(listenfd, LISTENQ);

	 // The parent will assign incoming client connections to children iteratively (max is LISTENQ)
   for (;;) {
      pid_t pid;
      struct sockaddr_in clientaddr;
      socklen_t clientaddr_len = sizeof(clientaddr);

			// Wait for a client connection
			sleep(1); // Wait a while
      connfd = Accept(listenfd, (struct sockaddr *) &clientaddr, &clientaddr_len); // Connect

			// Create a child process to handle client
      if((pid = fork()) == 0) {
				// Play again variable
				bool play_again = true;
				// Close listening socket
				Close(listenfd);
				// Debug warnning
				printf(">>>>> Client %d connected...\n", connfd);
				// Print some connection info
				print_conn_data(connfd,clientaddr);
				// While player wants to keep playing
				while (play_again == true) {
					// Get a random unique word from dictionary
					strcpy(selected_word, select_word(words, wrd_count));
					// Initialize game!
					startgame(connfd, selected_word, &play_again);
				}
				// Close accept socket
				Close(connfd);
				// Debug warnning
				printf(">>>>> ...client %d disconnected.\n\n", connfd);
				// Exit
				exit(0);
      }
			// Close connection (PARENT)
      Close(connfd);
   }
	 // Return
   return(0);
}

char * select_word(char ** words, int wrd_count) {
	int pos;
	// Pick random word
	do { pos = rand() % wrd_count;
	// Log message
	printf("Word found: \"%s\"\n", words[pos]);}
	// The word must be available
	while(words_availability[pos] == false);
	// Log message
	printf("Selected word is: \"%s\"\n", words[pos]);
	// Update status of picked word as unavailable
	words_availability[pos] = false;
	// Return picked word
	return words[pos];
}

void get_mode(int *mode, int connfd) {
	char recvline[MAX + 1];	// Receive msg
	int n;	// Size of msg
	bool valid = false;
	do {
		// Send menu to client
		send_msg(connfd, MENU);
		// Receive message from client (should be a char with mode)
		if((n = read(connfd, recvline, MAX)) > 0) recvline[n] = '\0';
		// Return string to int conversion
		*mode = (int)strtol(recvline, NULL, 10);
		// Check validity of mode
		if(*mode >= 1 && *mode <= 3) valid = true;
		else send_msg(connfd, "\nOpção inválida! Escolha outra.\n");
	} while(!valid);

}

//Aux Functions
void send_msg(int connfd, char msg[]) {
	// Send start message with menu
	write(connfd, msg, strlen(msg));
}
void help(){
	printf("%s", HELP);
}


void startgame(int connfd, char selected_word[], bool *play_again){
		// Game mode
		int mode;
		// Get mode from client
		get_mode(&mode, connfd);
		// Start the mode picked by client
    if(mode == 1) {
    	singleplayer(connfd, selected_word, &play_again);
    }else if(mode == 2){

		}
		else {
			char msg[] = "Game mode not implemented yet. Bye!\n";
			write(connfd, msg, strlen(msg));
		}
}

char * get_fields(int n) {
	char * fields = (char *)malloc((2*n+1)*sizeof(char));
	int i;
	// Generate string of underscores
  for(i = 0; i < 2*n; i++) {
      if(i % 2) fields[i] = ' ';
      else fields[i] = '_';
  }
	// Append finishing characters
  fields[i]='\n';
  fields[i+1]='\0';
	// Return
	return fields;
}

char * update_partial(char selected_word[], char picked, char partial[], bool *hit, int *count) {
	int n = strlen(selected_word);
	char * updated_partial = (char *)malloc((2*n+1)*sizeof(char));
	int * mask = (int *)calloc((2*n+1), sizeof(int));;
    int actualcount=0;

	strcpy(updated_partial, partial);

  for(int i = 0; i < n; i++) {
		if (picked == selected_word[i]) {
			mask[2*i] = 1;
            actualcount++;
            (*count)++;
		}
  }

	for(int i = 0; i < 2*n+1; i++) {
		if (mask[i] == 1) {
			updated_partial[i] = picked;
		}
  }

  if(actualcount > 0){
    *hit=true;
  }
  else{
    *hit= false;
    }
	// Return
	return updated_partial;
}

char * update_letters(char letters[], char letter_to_remv, bool *checkremove, bool *out_of_range) {
	char * updated_letters = (char *)malloc((ALPHABET_SZ+1)*sizeof(char));

	strcpy(updated_letters, letters);

	if (letter_to_remv >= ASCII_OFFSET && letter_to_remv < ASCII_OFFSET + ALPHABET_SZ) {
        if(updated_letters[letter_to_remv - ASCII_OFFSET] != '.'){
            updated_letters[letter_to_remv - ASCII_OFFSET] = '.';
            *checkremove = true;
        }else{
            *checkremove = false;
        }
	} else {
		*out_of_range = true;
	}

	return updated_letters;
}

int update_state(int lifes, int count, int numberletters) {
    if( count >= numberletters){
        return WIN;
    }
    else if(lifes <= 0 ){
        return LOSE;
    }

    return IN_PLAY;
}

void checkwin(int state, int connfd, char selected_word[]){

    if(state == LOSE){
        char message[MAX]="Forca! Acabaram suas tentativas...\nA palavra correta era ";
        strcat(message, selected_word);
        strcat(message, ". Você perdeu!\n");
        send_msg(connfd, message);
    }else if(state == WIN){
        char message[MAX]="Você adivinhou a palavra ";
        strcat(message, selected_word);
        strcat(message, ". Parabéns!\n");
        send_msg(connfd, message);
    }
    return;
}

void check_guess(char recvline[], char selected_word[], int *lifes, int *count) {
	// Compare guess to selected word
	int ans = strcmp(recvline, selected_word);
	// Word matches!
	if(ans == 0) *count = strlen(selected_word); // Count is highest
	// Word  doesn't match
	else *lifes = 0; // Lose all lives
}

void singleplayer(int connfd, char selected_word[], bool **play_again){
		char message[MAX];
		char word_sz[MIN_CHR];
		char partial_word[MIN_CHR];
		char available_letters[] = ALPHABET;
		char last_picked;
		int state = IN_PLAY;
    int count=0;
    int lifes=6;
    int numberletters=0;
		// Send start message to client
		send_msg(connfd, START_SINGLE_MSG);
		// Send word info message to client
    numberletters = (int)strlen(selected_word);
		sprintf(word_sz, "%d", numberletters);
		strcpy(message, "A palavra possui ");
		strcat(message, word_sz);
		strcat(message, " letras\n\n");
		send_msg(connfd, message);

		// Initialize partial word with fields only
		strcpy(partial_word, get_fields(strlen(selected_word)));

		do {
			// Initialization variables
			char available_letters_msg[MIN_CHR];
			char recvline[MIN_CHR];
			int n;

			// Send available letters of alphabet
			strcpy(available_letters_msg, "Letras disponíveis:\n");
			strcat(available_letters_msg, available_letters);
			strcat(available_letters_msg, "\n");
			send_msg(connfd, available_letters_msg);
			// Send fields with partial letters for word
			send_msg(connfd, partial_word);

			// Receive answer from client
			if((n = read(connfd, recvline, MIN_CHR)) > 0) recvline[n] = '\0';
			// Delete newline from words
			if(recvline[n-1] == '\n')
				recvline[n-1] = '\0';

			// Check actions based on client message
			if (state == IN_PLAY) {
        // Variables for checking if letter is right or wrong
        bool hit=false;
        bool checkremove = false;
				bool out_of_range = false;
				// If letter -> set last picked variable
				if(n == 2) {
            last_picked = recvline[0];
      			// Update available letters
      			strcpy(available_letters, update_letters(available_letters, last_picked, &checkremove, &out_of_range));
								// There is letter that can be removed
								if(checkremove){
          						// Update fields to send back to client
          						strcpy(partial_word, update_partial(selected_word, last_picked, partial_word, &hit, &count));
											// The letter doesn't match any from the word
                      if( hit == false){
                          sprintf(message, "\nA palavra não tem nenhuma letra '%c' .\n", recvline[0]);
                          send_msg(connfd, message);
                          lifes--;
                          sprintf(message, "Você possui %d vidas\n", lifes);
                          send_msg(connfd, message);
											// The letter matches!
                      }else{
                          send_msg(connfd, "\nVocê acertou a letra!\n");
                      }
									// Character out of A-Z range
                  } else if(out_of_range) {
											sprintf(message, "\nA letra '%c' não está no intervalo A-Z. Tente uma letra nesse limite.\n", recvline[0]);
											send_msg(connfd, message);
									// Repeated letter from A-Z
				} else {
                      sprintf(message, "\nA letra '%c' já foi utilizada.\n", recvline[0]);
                      send_msg(connfd, message);
                  }
					// Message is a word (Guess/"Chute")
          } else {
						// Check if client got the right guess
						check_guess(recvline,selected_word,&lifes,&count);
					}
					// Update as to WIN/LOSE/IN_PLAY state
          state = update_state(lifes, count, numberletters);
			}

			// printf("O numero de letras acertadas até o momento é: %d\n", count);

		} while(state == IN_PLAY);

    checkwin(state, connfd, selected_word);
		// @TODO -- Unique words from dictionary per client

		// Ask if player wants to play again
		send_msg(connfd,PLAY_AGAIN_MSG);
		// Answer variables
		char recvline[MIN_CHR]; int n;
		// Receive answer from client
		if((n = read(connfd, recvline, MIN_CHR)) > 0) recvline[n] = '\0';
		// Wants to play again
		if (recvline[0] == '1')
			**play_again = true;
		// Doesn't want to play again
		else
			**play_again = false;

    return;
}

char ** read_dictionary(int * lines) {
	 FILE * fp;				// file pointer
	 // int lines = 0;		// file line counter
	 size_t len = 0;	// line size
	 char ** words;		// array of lines

 	 // Open file containing dictionary
 	 fp = fopen("input.txt", "r");
 	 // If couldn't open file
 	 if(fp == NULL) exit(EXIT_FAILURE);
 	 // Count lines
 	 while(!feof(fp)) if(fgetc(fp) == '\n') (*lines)++;
 	 // Go back to beginning of file
 	 rewind(fp);
 	 // Allocate "lines" positions in array of words
 	 words = (char **)malloc(*lines * sizeof(char *));
 	 for(int i = 0; i < *lines; i++)
 	 		words[i] = (char *)malloc(MIN_CHR * sizeof(char));
 	 // Read lines
 	 int i = 0;
 	 while (getline(&(words[i]), &(len), fp) != -1) {
		 // Get actual word size (len is NOT)
		 int length = strlen(words[i]);
		 // Delete newline from words
		 if(words[i][length-1] == '\n')
      	words[i][length-1] = '\0';
		 i++;
	 }
 	 // Close file
 	 fclose(fp);
	 // Return words from dictionary
	 return words;
}

void print_conn_data(int connfd, struct sockaddr_in clientaddr) {
		// Variables
		socklen_t remoteaddr_len = sizeof(clientaddr);

		// Get client connection data
		if (getpeername(connfd, (struct sockaddr *) &clientaddr, &remoteaddr_len) == -1) {
		   perror("getpeername() failed");
		   return;
		}

		// Print client connection data
		printf("<%s:%d>\n", inet_ntoa(clientaddr.sin_addr),(int) ntohs(clientaddr.sin_port));
}
