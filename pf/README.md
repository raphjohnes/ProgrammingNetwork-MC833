# MC833 -Programação em Redes - Projeto final - Jogo da Forca  
Este trabalho tem com intuito a implementação do jogo da forca através de ferramentas
de cliente/servidor.


## Instalação
A primeira etapa para poder jogar, você deve compilar o código. Para isso, Digite
o comando abaixo no terminal na mesma página do jogo.

```shell
make
```
Você receberá a seguinte mensagem:
```shell
gcc -Wall -g servidor.c socket_helper.c  -o servidor
gcc -Wall -g cliente.c socket_helper.c  -o cliente
```

## Execução
Para que você possa iniciar o jogo, deve iniciar o servidor e o cliente. Sendo o servidor o intermediador principal do jogo e o cliente os jogadores.

### Servidor
Execução do servidor:
```shell
./servidor <PORT> <LISTENQ>
```
Exemplo:
```shell
./servidor 8000 10
```
### Cliente
Execução do cliente:
```shell
./cliente <IP> <PORT>
```

Exemplo:
```shell
./cliente 127.0.0.1 8000
```

## Regras do jogo
O jogo da forca possui algumas regras bem definidas. Dois personagens principais atuam neste jogo sendo: carrasco e o enforcado. Seu principal objetivo é adivinhar palavras com menor número de tentativas. Principais regras:

- Você terá um número limitado de vidas.
- Você poderá tentar chutar uma letra a cada rodada. A cada tentativa errada você ira perder uma vida. Caso acerte, uma letra será preenchida na palavra.
- Você poderá tentar chutar uma palavra. Caso você erre, perderá todas as vidas.


### Carrasco
O carrasco será o que fornecerá a palavra a ser advinhada, além de verificar se suas tentativas estão corretas.
### Enforcado
O enforcado será o que tentará advinhar a palavra através de letras ou palavra.\nModos de jogo:
### Modos de Jogo

Single player: Computador irá escolher uma palavra aleatória e enforcado deve tentar advinhar. Você possuirá 6 vidas.

Multiplayer: Um jogador irá ser o carrasco enquanto outros jogadores serão os enforcados. Você possuirá 4 vidas.


## Instruções do jogo
O fluxo do jogo deve seguir o seguinte caminho:

1. O carrasco informa ao enforcado quantas letras tem a palavra.
2. O enforcado então diz uma letra do alfabeto
3. Ocarrasco verifica se esta letra está contida na palavra:
4. O carrasco escolhe uma palavra sem mostrar para o enforcado
5. Se estiver, o mesmo preenche os espaços em branco correspondentes à letra
6. Caso não estiver, o carrasco desenha uma parte do corpo de um boneco na forca
7. Se o enforcado fizer a tentativa de adivinhar a palavra inteira e errar, então ele é executado e perde o jogo.
8. As tentativas se repetem até que todas as partes do corpo tenham sido desenhadas.


## Créditos
Desenvolvedores:
Alan Carvalho Corrales
Raphael Pontes Santana raphaellpontess@gmail.com

Agradecemos seu interesse por nosso jogo.
