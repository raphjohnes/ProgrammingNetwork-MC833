#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <netdb.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <time.h>
#include <unistd.h>
#include <arpa/inet.h>

#define LISTENQ 10
#define MAXDATASIZE 100

int main (int argc, char **argv) {
   int    listenfd, connfd, pid, port;
   struct sockaddr_in servaddr, clientaddr;
   char   sendBuffer[MAXDATASIZE] = "";
	 char   rcvBuffer[MAXDATASIZE] = "";
   char   serv_ip[MAXDATASIZE] = "";
   char   client_ip[MAXDATASIZE] = "";
   socklen_t client_addr_size;
   time_t ticks;
   struct tm* tm_info;

   // Associates time to ticks
   time(&ticks);

	// Creates socket descriptor
   if ((listenfd = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
      perror("socket");
      exit(1);
   }

	 // Clear
   memset(servaddr.sin_zero, '\0', sizeof servaddr.sin_zero);

	 // Set server port, address and protocol
   bzero(&servaddr, sizeof(servaddr));
   servaddr.sin_family      = AF_INET;
   servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
   servaddr.sin_port        = htons(atoi(argv[1]));

	 // Binds to socket
   if (bind(listenfd, (struct sockaddr *)&servaddr, sizeof(servaddr)) == -1) {
      perror("bind");
      exit(1);
   }

	 // Listens on socket
   if (listen(listenfd, LISTENQ) == -1) {
      perror("listen");
      exit(1);
   }

   // Prints server IP and port
   inet_ntop(AF_INET, &servaddr.sin_addr, serv_ip, sizeof(servaddr));
   printf("Servidor: %s:%s\n", serv_ip, argv[1]);

	 // Keeps waiting for connection
   for ( ; ; ) {
		 	// Waits to connect to a client
      if ((connfd = accept(listenfd, (struct sockaddr *) &clientaddr, &client_addr_size)) == -1 ) {
         perror("accept");
         exit(1);
      }

      // Gets connected client's IP and port
      port = (int) ntohs(clientaddr.sin_port);
      inet_ntop(AF_INET, &clientaddr.sin_addr, client_ip, sizeof(clientaddr));
      // Print logs
      tm_info = localtime(&ticks);
      printf("\n%s --> Cliente conectado: %s:%d\n", asctime(tm_info), client_ip, port);

			// Creates a child process
			pid = fork();

			// If child
			if (pid == 0) {
				// Closes listening socket
				close(listenfd);
				// Transfer and receive data from client
			 	fgets(sendBuffer, MAXDATASIZE, stdin);
				// Sends command client
			 	send(connfd, sendBuffer, strlen(sendBuffer), 0);
			 	// Receives message from client
			 	read(connfd, rcvBuffer, MAXDATASIZE);
				// Prints message
				fputs(rcvBuffer, stdout);
				// Close connection
				close(connfd);
        // Warn connection closed
        printf(" --> Conexao fechada.");

				// Terminates
				exit(0);
			// If parent
			}

			// Parent closes connected socket
			close(connfd);

			// TODO if this is still useful
			// write(connfd, buf, strlen(buf));
   }

   return(0);
}
