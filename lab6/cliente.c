#include <sys/socket.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <stdio.h>
#include <netdb.h>
#include <string.h>
#include <errno.h>
#include <strings.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/select.h>
#include <sys/time.h>
#include "basic.h"
#include "socket_helper.h"

#define MAXLINE 4096
#define EXIT_COMMAND "exit\n"


void doit(FILE *fp, int sockfd);
int max(int a, int b);
void err_quit(char *msg);

int main(int argc, char **argv) {
   int    client_port, sockfd;
   char * client_ip;
   char   error[MAXLINE + 1];
   struct sockaddr_in servaddr;

   if (argc != 3) {
      strcpy(error,"uso: ");
      strcat(error,argv[0]);
      strcat(error," <IPaddress, Port>");
      perror(error);
      exit(1);
   }

   client_ip = argv[1];
   client_port = atoi(argv[2]);

   sockfd = Socket(AF_INET, SOCK_STREAM, 0);

   servaddr = ClientSockaddrIn(AF_INET, client_ip, client_port);

   Connect(sockfd, (struct sockaddr *) &servaddr, sizeof(servaddr));

   doit(stdin, sockfd);

   exit(0);
}

int max(int a, int b) {
	if(a > b) return a;
	else return b;
}

void err_quit(char *msg) {
	printf("%s\n", msg);
	exit(EXIT_FAILURE);
}
ssize_t                         /* Write "n" bytes to a descriptor. */
writen(int fd, const void *vptr, size_t n)
{
     size_t nleft;
     ssize_t nwritten;
     const char *ptr;

     ptr = vptr;
     nleft = n;
     while (nleft > 0) {
         if ( (nwritten = write(fd, ptr, nleft)) <= 0) {
             if (nwritten < 0 && errno == EINTR)
                 nwritten = 0;   /* and call write() again */
             else
                 return (-1);    /* error */
          }

          nleft -= nwritten;
          ptr += nwritten;
     }
     return (n);
}


void doit(FILE *fp, int sockfd) {
   char   receive[MAXLINE + 1];
   char   response[MAXLINE + 1];
   int    n, maxfdp1, stdineof;
	 fd_set      rset;
     stdineof = 0;
	 FD_ZERO(&rset);
   for ( ; ; ) {
    if (stdineof == 0)
        FD_SET(fileno(fp), &rset);
    FD_SET(sockfd, &rset);
    maxfdp1 = max(fileno(fp), sockfd) + 1;
    select(maxfdp1, &rset, NULL, NULL, NULL);

    if (FD_ISSET(sockfd, &rset)) {  /* socket is readable */
        if ( (n = read(sockfd, receive, MAXLINE)) == 0) {
            if (stdineof == 1)
                return;     /* normal termination */
            else
                err_quit("str_cli: server terminated prematurely");
        }

        write(fileno(stdout), receive, n);
    }

    if (FD_ISSET(fileno(fp), &rset)) {  /* input is readable */
        if ( (n = read(fileno(fp), response, MAXLINE)) == 0) {
            stdineof = 1;
            shutdown(sockfd, SHUT_WR);  /* send FIN */
            FD_CLR(fileno(fp), &rset);
            continue;
        }

        writen(sockfd, response, n);
    }
}
}
