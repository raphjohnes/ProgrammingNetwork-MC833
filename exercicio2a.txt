bash-4.4$ ping www.google.com -c 5
PING www.google.com (172.217.29.100) 56(84) bytes of data.
64 bytes from gru09s19-in-f100.1e100.net (172.217.29.100): icmp_seq=1 ttl=55 time=13.1 ms
64 bytes from gru09s19-in-f100.1e100.net (172.217.29.100): icmp_seq=2 ttl=55 time=13.0 ms
64 bytes from gru09s19-in-f100.1e100.net (172.217.29.100): icmp_seq=3 ttl=55 time=13.2 ms
64 bytes from gru09s19-in-f100.1e100.net (172.217.29.100): icmp_seq=4 ttl=55 time=13.7 ms
64 bytes from gru09s19-in-f100.1e100.net (172.217.29.100): icmp_seq=5 ttl=55 time=13.1 ms

--- www.google.com ping statistics ---
5 packets transmitted, 5 received, 0% packet loss, time 4006ms
rtt min/avg/max/mdev = 13.097/13.286/13.730/0.262 ms
bash-4.4$ export PATH=/usr/bin:/bin:/usr/sbin:/sbin:${PATH}
bash-4.4$ ping www.google.com -c 5
PING www.google.com (172.217.29.100) 56(84) bytes of data.
64 bytes from gru09s19-in-f100.1e100.net (172.217.29.100): icmp_seq=1 ttl=55 time=13.1 ms
64 bytes from gru09s19-in-f100.1e100.net (172.217.29.100): icmp_seq=2 ttl=55 time=13.1 ms
64 bytes from gru09s19-in-f100.1e100.net (172.217.29.100): icmp_seq=3 ttl=55 time=13.2 ms
64 bytes from gru09s19-in-f100.1e100.net (172.217.29.100): icmp_seq=4 ttl=55 time=13.2 ms
64 bytes from gru09s19-in-f100.1e100.net (172.217.29.100): icmp_seq=5 ttl=55 time=13.1 ms

--- www.google.com ping statistics ---
5 packets transmitted, 5 received, 0% packet loss, time 4005ms
rtt min/avg/max/mdev = 13.149/13.198/13.268/0.042 ms
bash-4.4$ ping www.unicamp.br -c 5
PING cerejeira.unicamp.br (143.106.10.174) 56(84) bytes of data.
64 bytes from cerejeira.unicamp.br (143.106.10.174): icmp_seq=1 ttl=59 time=0.842 ms
64 bytes from cerejeira.unicamp.br (143.106.10.174): icmp_seq=2 ttl=59 time=0.970 ms
64 bytes from cerejeira.unicamp.br (143.106.10.174): icmp_seq=3 ttl=59 time=0.792 ms
64 bytes from cerejeira.unicamp.br (143.106.10.174): icmp_seq=4 ttl=59 time=0.826 ms
64 bytes from cerejeira.unicamp.br (143.106.10.174): icmp_seq=5 ttl=59 time=0.865 ms

--- cerejeira.unicamp.br ping statistics ---
5 packets transmitted, 5 received, 0% packet loss, time 4068ms
rtt min/avg/max/mdev = 0.792/0.859/0.970/0.060 ms
bash-4.4$ traceroute unicamp.br
unicamp.br: Name or service not known
Cannot handle "host" cmdline arg `unicamp.br' on position 1 (argc 1)
bash-4.4$ traceroute www.unicamp.br
traceroute to www.unicamp.br (143.106.10.174), 30 hops max, 60 byte packets
 1  gateway (143.106.16.62)  0.770 ms  0.758 ms  1.022 ms
 2  172.16.10.253 (172.16.10.253)  0.697 ms  0.961 ms  0.953 ms
 3  area3-gw.unicamp.br (143.106.1.129)  0.638 ms  0.619 ms  0.874 ms
 4  ptp-ncc-nbs.unicamp.br (143.106.199.9)  6.018 ms  6.001 ms  5.977 ms
 5  dmz-gw.unicamp.br (143.106.2.52)  0.457 ms  0.447 ms  0.675 ms
 6  cerejeira.unicamp.br (143.106.10.174)  0.666 ms  0.670 ms  0.666 ms
bash-4.4$ traceroute www.google.com
traceroute to www.google.com (216.58.202.68), 30 hops max, 60 byte packets
 1  gateway (143.106.16.62)  0.499 ms  0.822 ms  1.105 ms
 2  172.16.10.253 (172.16.10.253)  0.776 ms  0.779 ms  1.070 ms
 3  area3-gw.unicamp.br (143.106.1.129)  0.738 ms  0.737 ms  1.022 ms
 4  ptp-nct-nbs.unicamp.br (143.106.199.13)  0.339 ms  0.683 ms  0.671 ms
 5  ptp-ncc-nct.unicamp.br (143.106.199.1)  12.183 ms  12.199 ms as15169.saopaulo.sp.ix.br (187.16.216.55)  3.565 ms
 6  108.170.245.193 (108.170.245.193)  3.557 ms  3.684 ms 108.170.245.225 (108.170.245.225)  3.054 ms
 7  108.170.245.225 (108.170.245.225)  3.077 ms 108.170.226.227 (108.170.226.227)  3.338 ms 108.170.245.225 (108.170.245.225)  3.026 ms
 8  gru10s11-in-f4.1e100.net (216.58.202.68)  3.313 ms 108.170.226.227 (108.170.226.227)  4.014 ms 108.170.226.151 (108.170.226.151)  4.340 ms
bash-4.4$ ping www.unicamp.br -c 5 -s 496
PING cerejeira.unicamp.br (143.106.10.174) 496(524) bytes of data.
504 bytes from cerejeira.unicamp.br (143.106.10.174): icmp_seq=1 ttl=59 time=0.957 ms
504 bytes from cerejeira.unicamp.br (143.106.10.174): icmp_seq=2 ttl=59 time=0.975 ms
504 bytes from cerejeira.unicamp.br (143.106.10.174): icmp_seq=3 ttl=59 time=0.929 ms
504 bytes from cerejeira.unicamp.br (143.106.10.174): icmp_seq=4 ttl=59 time=1.00 ms
504 bytes from cerejeira.unicamp.br (143.106.10.174): icmp_seq=5 ttl=59 time=0.978 ms

--- cerejeira.unicamp.br ping statistics ---
5 packets transmitted, 5 received, 0% packet loss, time 4005ms
rtt min/avg/max/mdev = 0.929/0.968/1.001/0.024 ms
bash-4.4$ ping www.google.com -c 5 -s 496
PING www.google.com (172.217.29.100) 496(524) bytes of data.
72 bytes from gru09s19-in-f100.1e100.net (172.217.29.100): icmp_seq=1 ttl=55 (truncated)
72 bytes from gru09s19-in-f100.1e100.net (172.217.29.100): icmp_seq=2 ttl=55 (truncated)
72 bytes from gru09s19-in-f100.1e100.net (172.217.29.100): icmp_seq=3 ttl=55 (truncated)
72 bytes from gru09s19-in-f100.1e100.net (172.217.29.100): icmp_seq=4 ttl=55 (truncated)
72 bytes from gru09s19-in-f100.1e100.net (172.217.29.100): icmp_seq=5 ttl=55 (truncated)

--- www.google.com ping statistics ---
5 packets transmitted, 5 received, 0% packet loss, time 4004ms
rtt min/avg/max/mdev = 13.249/13.321/13.367/0.110 ms
bash-4.4$ ping www.google.com -c 5 -s 496
PING www.google.com (172.217.29.100) 496(524) bytes of data.
72 bytes from rio01s24-in-f4.1e100.net (172.217.29.100): icmp_seq=1 ttl=55 (truncated)
72 bytes from rio01s24-in-f4.1e100.net (172.217.29.100): icmp_seq=2 ttl=55 (truncated)
72 bytes from rio01s24-in-f4.1e100.net (172.217.29.100): icmp_seq=3 ttl=55 (truncated)
72 bytes from rio01s24-in-f4.1e100.net (172.217.29.100): icmp_seq=4 ttl=55 (truncated)
72 bytes from rio01s24-in-f4.1e100.net (172.217.29.100): icmp_seq=5 ttl=55 (truncated)

--- www.google.com ping statistics ---
5 packets transmitted, 5 received, 0% packet loss, time 4005ms
rtt min/avg/max/mdev = 13.169/13.272/13.355/0.151 ms
bash-4.4$ ping -c 5 -s 496 www.google.com
PING www.google.com (172.217.29.100) 496(524) bytes of data.
72 bytes from gru09s19-in-f100.1e100.net (172.217.29.100): icmp_seq=1 ttl=55 (truncated)
72 bytes from gru09s19-in-f100.1e100.net (172.217.29.100): icmp_seq=2 ttl=55 (truncated)
72 bytes from gru09s19-in-f100.1e100.net (172.217.29.100): icmp_seq=3 ttl=55 (truncated)
72 bytes from gru09s19-in-f100.1e100.net (172.217.29.100): icmp_seq=4 ttl=55 (truncated)
72 bytes from gru09s19-in-f100.1e100.net (172.217.29.100): icmp_seq=5 ttl=55 (truncated)

--- www.google.com ping statistics ---
5 packets transmitted, 5 received, 0% packet loss, time 4005ms
rtt min/avg/max/mdev = 13.200/13.319/13.446/0.084 ms
bash-4.4$ ping -c 5 www.lrc.ic.unicamp.br
PING lrc-gw.ic.unicamp.br (143.106.7.163) 56(84) bytes of data.

--- lrc-gw.ic.unicamp.br ping statistics ---
5 packets transmitted, 0 received, 100% packet loss, time 4097ms

bash-4.4$ ifconfig
docker0: flags=4099<UP,BROADCAST,MULTICAST>  mtu 1500
        inet 172.17.0.1  netmask 255.255.0.0  broadcast 0.0.0.0
        ether 02:42:7c:58:3d:02  txqueuelen 0  (Ethernet)
        RX packets 0  bytes 0 (0.0 B)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 0  bytes 0 (0.0 B)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

eno1: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        inet 143.106.16.48  netmask 255.255.255.192  broadcast 143.106.16.63
        inet6 fe80::36e6:d7ff:fefc:2b48  prefixlen 64  scopeid 0x20<link>
        ether 34:e6:d7:fc:2b:48  txqueuelen 1000  (Ethernet)
        RX packets 1089203  bytes 1275131226 (1.1 GiB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 874276  bytes 594295988 (566.7 MiB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0
        device interrupt 20  memory 0xf7c00000-f7c20000  

lo: flags=73<UP,LOOPBACK,RUNNING>  mtu 65536
        inet 127.0.0.1  netmask 255.0.0.0
        inet6 ::1  prefixlen 128  scopeid 0x10<host>
        loop  txqueuelen 1000  (Local Loopback)
        RX packets 0  bytes 0 (0.0 B)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 0  bytes 0 (0.0 B)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

virbr0: flags=4099<UP,BROADCAST,MULTICAST>  mtu 1500
        inet 192.168.122.1  netmask 255.255.255.0  broadcast 192.168.122.255
        ether 52:54:00:c7:84:7d  txqueuelen 1000  (Ethernet)
        RX packets 0  bytes 0 (0.0 B)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 0  bytes 0 (0.0 B)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

bash-4.4$ route
Kernel IP routing table
Destination     Gateway         Genmask         Flags Metric Ref    Use Iface
default         gateway         0.0.0.0         UG    0      0        0 eno1
143.106.16.0    0.0.0.0         255.255.255.192 U     0      0        0 eno1
link-local      0.0.0.0         255.255.0.0     U     1002   0        0 eno1
172.17.0.0      0.0.0.0         255.255.0.0     U     0      0        0 docker0
192.168.122.0   0.0.0.0         255.255.255.0   U     0      0        0 virbr0
bash-4.4$ nslookup
> www.facebook.com domain
Server:		143.106.16.144
Address:	143.106.16.144#53

Non-authoritative answer:
www.facebook.com	canonical name = star-z-mini.c10r.facebook.com.
Name:	star-z-mini.c10r.facebook.com
Address: 31.13.85.38
Name:	star-z-mini.c10r.facebook.com
Address: 2a03:2880:f105:86:face:b00c:0:50fb
> lserver www.facebook.com
Default server: www.facebook.com
Address: 31.13.85.38#53
Default server: www.facebook.com
Address: 2a03:2880:f105:86:face:b00c:0:50fb#53
> view
The 'view' command is not implemented.
> host www.facebook.com
;; connection timed out; no servers could be reached
> exit       

bash-4.4$ traceroute www.facebook.com
traceroute to www.facebook.com (31.13.85.38), 30 hops max, 60 byte packets
 1  gateway (143.106.16.62)  0.631 ms  0.865 ms  0.854 ms
 2  172.16.10.253 (172.16.10.253)  0.788 ms  0.799 ms  1.029 ms
 3  area3-gw.unicamp.br (143.106.1.129)  0.720 ms  0.711 ms  0.661 ms
 4  * ptp-ncc-nbs.unicamp.br (143.106.199.9)  0.592 ms  0.571 ms
 5  as32934.saopaulo.sp.ix.br (187.16.220.240)  5.203 ms  5.171 ms  5.154 ms
 6  po221.asw04.gru2.tfbnw.net (31.13.31.200)  3.910 ms po221.asw03.gru2.tfbnw.net (31.13.31.198)  3.287 ms  3.514 ms
 7  129.134.33.137 (129.134.33.137)  3.444 ms 129.134.34.5 (129.134.34.5)  3.438 ms 129.134.33.71 (129.134.33.71)  3.987 ms
 8  173.252.67.67 (173.252.67.67)  3.391 ms 173.252.67.105 (173.252.67.105)  3.942 ms 173.252.67.13 (173.252.67.13)  3.923 ms
 9  edge-star-z-mini-shv-01-gru2.facebook.com (31.13.85.38)  3.895 ms  4.452 ms  3.270 ms
bash-4.4$ traceroute www.ethz.ch?
www.ethz.ch?: Name or service not known
Cannot handle "host" cmdline arg `www.ethz.ch?' on position 1 (argc 1)
bash-4.4$ traceroute www.ethz.ch
traceroute to www.ethz.ch (129.132.19.216), 30 hops max, 60 byte packets
 1  gateway (143.106.16.62)  0.863 ms  0.833 ms  1.158 ms
 2  172.16.10.253 (172.16.10.253)  0.729 ms  0.753 ms  1.089 ms
 3  area3-gw.unicamp.br (143.106.1.129)  3.776 ms  3.759 ms  3.950 ms
 4  ptp-ncc-nbs.unicamp.br (143.106.199.9)  0.580 ms *  0.534 ms
 5  * * *
 6  sp-sp2.bkb.rnp.br (200.143.253.37)  3.105 ms  2.397 ms  2.354 ms
 7  br-rnp.redclara.net (200.0.204.213)  4.263 ms  3.888 ms  3.872 ms
 8  us-br.redclara.net (200.0.204.9)  119.167 ms  118.064 ms  118.038 ms
 9  redclara-gw.par.fr.geant.net (62.40.125.168)  219.247 ms  219.279 ms  219.688 ms
10  ae2.mx1.gen.ch.geant.net (62.40.98.153)  226.046 ms  226.067 ms  226.483 ms
11  swice1-100ge-0-3-0-1.switch.ch (62.40.124.22)  226.453 ms  228.380 ms  227.938 ms
12  swiCE4-100GE-0-0-0-0.switch.ch (130.59.36.6)  226.432 ms  227.479 ms  227.089 ms
13  swiZH1-100GE-0-1-0-1.switch.ch (130.59.36.94)  231.283 ms  230.569 ms  230.788 ms
14  swiEZ3-100GE-0-1-0-4.switch.ch (130.59.38.109)  230.465 ms  230.618 ms  230.492 ms
15  rou-gw-lee-tengig-to-switch.ethz.ch (192.33.92.1)  231.588 ms  230.268 ms  230.494 ms
16  rou-fw-rz-rz-gw.ethz.ch (192.33.92.169)  230.731 ms  230.407 ms  230.627 ms
17  * * *
18  * * *
19  * * *
20  * * *
21  * * *
22  * * *
23  * * *
24  * * *
25  * * *
26  * * *
27  * * *
28  * * *
29  * * *
30  * * *
bash-4.4$ ^C
    
