#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <netdb.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <time.h>
#include <unistd.h>

#define LISTENQ 10
#define MAXDATASIZE 100

int main (int argc, char **argv) {
   int    listenfd, connfd;
   struct sockaddr_in servaddr;
   // char   buf[MAXDATASIZE];
   struct sockaddr_in peeraddr; // Stores client address and port info created by socket
   socklen_t peeraddr_size; // Stores client address size
   // time_t ticks;

   if ((listenfd = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
      perror("socket");
      exit(1);
   }

   memset(servaddr.sin_zero, '\0', sizeof servaddr.sin_zero);

   bzero(&servaddr, sizeof(servaddr));
   servaddr.sin_family      = AF_INET;
   servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
   servaddr.sin_port        = htons(8000);

   if (bind(listenfd, (struct sockaddr *)&servaddr, sizeof(servaddr)) == -1) {
      perror("bind");
      exit(1);
   }

   if (listen(listenfd, LISTENQ) == -1) {
      perror("listen");
      exit(1);
   }

   for ( ; ; ) {
      if ((connfd = accept(listenfd, (struct sockaddr *) NULL, NULL)) == -1 ) {
         perror("accept");
         exit(1);
      }

      //  -- QUESTION 4 -- servidor.c
      // // This part is to get client's address and port info
      peeraddr_size = sizeof(peeraddr);
      getpeername(connfd, (struct sockaddr *) &peeraddr, (socklen_t *) &peeraddr_size);
      // // Print client's address and port
      printf("sock_peer_IP := %u\n", peeraddr.sin_addr.s_addr);
      printf("sock_peer_port := %u\n", peeraddr.sin_port);

      //  -- QUESTION 5 -- servidor.c
      // read(connfd, buf, MAXDATASIZE);
      // printf("%s\n", buf);
      // send(connfd, buf, strlen(buf), 0);

      
      // ticks = time(NULL);
      // snprintf(buf, sizeof(buf), "%.24s\r\n", ctime(&ticks));
      // write(connfd, buf, strlen(buf));



      close(connfd);
   }
   return(0);
}
